package com.happydave.contactgrid;

/**
 * Created by HappyDave on 8/11/2017
 */

class ContactGridModel {
    private int photo;
    private String name;
    private String phone;

    public ContactGridModel(Builder builder) {
        if (builder.photo == 0) {
            photo = R.drawable.contacts_social_network;
        } else {
            photo = builder.photo;
        }
        name = builder.name;
        phone = builder.phone;
    }

    public int getPhoto() {
        return photo;
    }

    public String getName() {
        return name;
    }

    /***********************/
    /**  Builder Pattern  **/
    /***********************/

    public static class Builder {
        private int photo;
        private String name = "";
        private String phone;

        public Builder(String phone) {
            this.phone = phone;
        }

        public Builder photo(int photo) {
            this.photo = photo;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public ContactGridModel build() {
            return new ContactGridModel(this);
        }
    }
}
