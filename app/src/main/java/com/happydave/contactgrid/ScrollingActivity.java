package com.happydave.contactgrid;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class ScrollingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);

        List<ContactGridModel> contacts = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            contacts.add(new ContactGridModel.Builder("666666666").name("Paula Gonzalez").photo(R.drawable.untitled).build());
        }

        final ContactGridAdapter contactGridAdapter = new ContactGridAdapter(this, contacts);
        contactGridAdapter.setMode(ContactGridModeType.EDIT);
        contactGridAdapter.setOnAddContactClickListener(new ContactGridAdapter.OnAddContactClickListener() {
            @Override
            public void onAddContactClick() {
                contactGridAdapter.add(new ContactGridModel.Builder("666666666").name("Paula Gonzalez").photo(R.drawable.untitled).build());
            }
        });

        contactGridAdapter.setOnContactClickListener(new ContactGridAdapter.OnContactClickListener() {
            @Override
            public void onContactClick(ContactGridModel contact) {
                contactGridAdapter.remove(contact);
            }
        });

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.contact_list);
        //recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(contactGridAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
