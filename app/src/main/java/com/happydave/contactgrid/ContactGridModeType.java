package com.happydave.contactgrid;

/**
 * Created by HappyDave on 8/12/2017
 */

public enum ContactGridModeType {
    NONE,
    EDIT,
    REMOVE,
    INSPECT
}
