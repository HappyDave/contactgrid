package com.happydave.contactgrid;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by HappyDave on 8/11/2017
 */

public class ContactGridAdapter extends RecyclerView.Adapter<ContactGridAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    private List<ContactGridModel> mData = new ArrayList<>();
    private ContactGridModeType modeType = ContactGridModeType.NONE;
    private ContactGridModel addContactModel = new ContactGridModel.Builder("666223344").name("Añadir destinatario").photo(R.drawable.plus).build();

    private OnContactClickListener onContactClickListener;
    private OnAddContactClickListener onAddContactClickListener;


    public ContactGridAdapter(Context context, List<ContactGridModel> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.row_contact_grid, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ContactGridModel contact = mData.get(position);

        switch (modeType) {
            case EDIT:
                holder.icon.setImageResource(R.drawable.error);
                if (position == 0) {
                    holder.icon.setVisibility(GONE);
                } else {
                    holder.icon.setVisibility(VISIBLE);
                }
                break;

            case REMOVE:
                holder.icon.setImageResource(R.drawable.error);
                break;

            case INSPECT:
                holder.icon.setImageResource(R.drawable.eye);
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) holder.icon.getLayoutParams();
                lp.addRule(RelativeLayout.ALIGN_BOTTOM, R.id.image);
                holder.icon.setLayoutParams(lp);
                break;

            case NONE:
            default:
                holder.icon.setVisibility(GONE);
                break;
        }

        holder.image.setImageResource(contact.getPhoto());
        holder.title.setText(contact.getName());
    }

    public void setMode(ContactGridModeType modeType) {
        this.modeType = modeType;

        switch (modeType) {
            case EDIT:
                mData.add(0, addContactModel);
                break;

            case INSPECT:
            case REMOVE:
            case NONE:
            default:
                mData.remove(addContactModel);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void add(ContactGridModel item) {
        mData.add(item);
        notifyDataSetChanged();
    }

    public void remove(ContactGridModel item) {
        mData.remove(item);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView image;
        CircleImageView icon;
        TextView title;

        public ViewHolder(View itemView) {
            super(itemView);

            image = (CircleImageView) itemView.findViewById(R.id.image);
            icon = (CircleImageView) itemView.findViewById(R.id.icon);
            title = (TextView) itemView.findViewById(R.id.title);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClicked();
                }
            });
        }

        private void onItemClicked() {
            if (ContactGridModeType.EDIT == modeType && getAdapterPosition() == 0) {
                notifyOnAddContactClick();
            } else {
                notifyOnContactClick(mData.get(getAdapterPosition()));
            }
        }
    }

    public interface OnContactClickListener {
        void onContactClick(ContactGridModel contact);
    }

    public void setOnContactClickListener(OnContactClickListener onContactClickListener) {
        this.onContactClickListener = onContactClickListener;
    }

    public void notifyOnContactClick(ContactGridModel contact) {
        if (onContactClickListener != null) {
            onContactClickListener.onContactClick(contact);
        }
    }


    public interface OnAddContactClickListener {
        void onAddContactClick();
    }

    public void setOnAddContactClickListener(OnAddContactClickListener onAddContactClickListener) {
        this.onAddContactClickListener = onAddContactClickListener;
    }

    private void notifyOnAddContactClick() {
        if (onAddContactClickListener != null) {
            onAddContactClickListener.onAddContactClick();
        }
    }
}
